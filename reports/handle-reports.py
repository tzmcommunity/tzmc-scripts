import os
import sys
import datetime

print ("SRC:" + str(sys.argv[1]))

dest = sys.argv[2]
print ("DEST: " + dest)

for root, dirs, files in os.walk(dest, topdown=False):
    for name in files:
        if ".md" in name:
            path = os.path.join(root, name)
            print ("removing " + path)
            os.remove(os.path.join(root, name))
        
for root, dirs, files in os.walk(sys.argv[1]):
    for name in files:
        org_file_name = os.path.join(root, name)
        print(org_file_name)

        branch_name= root.split("/")[2]
        print("")
        print("NEXT FILE " + org_file_name)
        print("root " + str(root))
        print("branch name " + str(branch_name))        
        print("name " + str(name))
        
        
        with open(org_file_name) as f:
            org_content = f.read()
        
        print("-------------------------")
        print("org_content: " + str(org_content))
        print("-------------------------")
        
        sdate = name.split(".")[0]
        
        final_content = ""
        final_content += "---\n"
        final_content += "layout: branch-report\n"
        final_content += "date: " + sdate + "\n"
        final_content += "title: " + branch_name + " " + sdate + "\n"
        final_content += "---\n"
        final_content += org_content
        
        final_file_name = branch_name + "-" + sdate + ".md"
        print("final_file_name " + final_file_name)
        print("-------------------------")
        print("final_content: " + str(final_content))
        print("-------------------------")

        text_file = open(dest + "/" + final_file_name, "w")
        n = text_file.write(final_content)
        text_file.close()
