#!/bin/bash

cd $(dirname $0)
pwd

docker build .. -t tzmc-reports
docker run -v "$(pwd)/src:/reports-src" -v "$(pwd)/dest:/reports-dst" tzmc-reports
